module.exports = {

	app : {
		src: [
            'src/js/common.js'
			// 'js/dev/functions/*.js',
			// 'js/dev/components/*.js',
			// 'js/dev/app.js'
		],
		dest: 'js/dev/main-app.js',
		options: {
			sourceMap: true
		}
	}

};