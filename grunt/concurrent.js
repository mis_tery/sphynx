module.exports = {

  dev:{
    tasks: ['watch:sass','watch:concat'],
    options: {
        logConcurrentOutput: true
    }
  },

  staging:{
    tasks: ['watch:sass','watch:postcss','watch:concat'],
    options: {
        logConcurrentOutput: true
    }
  },

  prod:{
    tasks: ['watch:sass','watch:postcss','watch:concat','watch:min_app'],
    options: {
        logConcurrentOutput: true
    }
  },

  lib: {
    tasks: ['uglify:lib','notify:lib_uglify'],
    options: {
        logConcurrentOutput: true
    }
  },

}