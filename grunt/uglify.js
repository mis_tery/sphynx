module.exports = {

	lib: {
		files: {
			'js/libs.min.js' : [
				'bower_components/jquery/dist/jquery.min.js',
				'bower_components/slick-carousel/slick/slick.min.js',
				'bower_components/bootstrap-sass/assets/javascripts/bootstrap/transition.js',
				'bower_components/bootstrap-sass/assets/javascripts/bootstrap/collapse.js'
                // 'bower_components/jquery/dist/jquery.min.js',
                // 'bower_components/jquery-ui/jquery-ui.min.js',
                // // 'bower_components/jquery.ui.touch-punch/dist/jquery.ui.touch-punch.min.js',
                // 'bower_components/modernizr/modernizr.js',
		    	// 'bower_components/svg4everybody/dist/svg4everybody.min.js',
			    // // 'bower_components/fastclick/lib/fastclick.js',
			    // 'bower_components/bootstrap-sass/assets/javascripts/bootstrap.js',
			    // 'bower_components/outdated-browser/outdatedbrowser/outdatedbrowser.min.js',
			    // 'bower_components/parsleyjs/dist/parsley.min.js',
                // 'bower_components/jquery_lazyload/jquery.lazyload.js',
			    // 'bower_components/slick.js/slick/slick.js',
			    // 'bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js',
                // 'bower_components/gsap/src/minified/TweenMax.min.js',
                // // 'src/js/vendor/animation.gsap.js',
			    // 'js/dev/vendor/jquery.smoothState.min.js',
			],
			'js/dev/main-app.js': [
                'src/js/common.js'
			]
		}
	}};