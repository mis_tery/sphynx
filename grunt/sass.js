module.exports = {
    // Development settings
    dev: {
        options: {
            outputStyle: 'nested',
            sourceMap: true
        },
        files: [{
            expand: true,
            cwd: 'src/scss',
            src: ['**/*.scss'],
            dest: 'css',
            ext: '.css'
        }]
    }
};