module.exports = {
  options: {
    map: true, // inline sourcemaps

    processors: [
      require('pixrem')(), // add fallbacks for rem units
      require('autoprefixer')({browsers: 'last 2 versions, IE >= 8, Safari >= 7' }), // add vendor prefixes
      require('cssnano')() // minify the result
    ]
  },
  dist: {
    // src: 'css/style.css',
    // dest: 'css/style.min.css'
    files: { 'css/style.min.css' : 'css/style.css'},
  }
}