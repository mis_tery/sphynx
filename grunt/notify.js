module.exports = {
	sass: {
		options: {
			title: 'SASS TASK COMPLETE',
			message: 'style.scss has been compiled to style.css'
		}
	},

	postcss: {
		options: {
			title: 'POSTCSS TASK COMPLETE',
			message: 'style.css has been prefixed'
		}
	},

	postcss_min: {
		options: {
			title: 'POSTCSS TASK COMPLETE',
			message: 'style.css has been prefixed and minified'
		}
	},
	
	concat: {
		options: {
			title: 'JS CONCAT TASK COMPLETE',
			message: 'main-combined.js have been generated'
		}
	},
	
	app_concat: {
		options: {
			title: 'JS CONCAT TASK COMPLETE',
			message: '*.js has been complied to app.js'
		}
	},
	app_uglify: {
		options: {
			title: 'APP UGLIFY TASK COMPLETE',
			message: 'app.js has been minified'
		}
	},
	lib_uglify: {
		options: {
			title: 'LIB UGLIFY TASK COMPLETE',
			message: 'libs.min.js has been created/updated'
		}
	},
	single_files_uglify: {
		options: {
			title: 'SINGLE FILES UGLIFY TASK COMPLETE',
			message: 'home.js have been minified'
		}
	}
};