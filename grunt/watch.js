module.exports = {
    // watch scss files
	sass: {
		files: ['src/scss/**/*.scss'],
		tasks: ['sass:dev','notify:sass'],
	},
	postcss: {
		files: ['css/*.css'],
		// tasks: ['postcss:default', 'notify:postcss']
		tasks: ['postcss', 'notify:postcss']
	},
	// concat app.js
	concat: {
		files: ['src/js/**/*.js'],
		tasks: ['concat','notify:concat']
	},
	// minify app.js
	min_app: {
		files: ['public/js/dist/app.js'],
		tasks: ['uglify:app','notify:app_uglify']
	},
};