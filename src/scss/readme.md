
                          Sass Structure

style.scss
----------
Importing main.scss + other styles (Bootstrap, Fontawesome, etc.). 
Grunt converts this file to css.


_main.scss
----------
Importing all the project styles.


layout/
-------
  Folder including _base.scss (general styles used everywhere: html, body, text, etc.) and any other files styling a part of the layout (header, footer, sidebar, blog, cms-area, etc.)

pages/
------
  Including specific styles for pages with a different layout (home, login page, etc.)

components/
-----------
  Any element that can be used anywhere in the website (breadcrumbs, social sharing, dropdown, slider, modal, etc.). 
  _all.scss includes all the components and it's imported by _main.scss.

helpers/
--------
  Variables (text, colors, etc.), mixins (transitions, border-radius, etc.) and other helpers (clearfix, etc.).
  _all.scss includes all the helpers and it's imported by _main.scss.

vendors/
--------
  Styles of components developed by other people (outdated, blueimp gallery etc.).

