$(function(){
    var $window = $(window),
        $header = $('#main_header'),
        $content = $('.main-wrapper'),
        gridFloatBreakpoint = 992;

    $window.scroll(function() {

        var top = window.pageYOffset || document.documentElement.scrollTop;

        if ($window.width() < gridFloatBreakpoint && top > $header.height()*2) {
            $content.addClass('fixed');
        } else {
            $content.removeClass('fixed');
        }
    });

    $('.slider').slick({
        slidesToShow: 1,
        arrows: true,
        dots: true,
        prevArrow: '<button type="button" class="slider-nav prev slick-prev icon-arrow-left"></button>',
        nextArrow: '<button type="button" class="slider-nav next slick-next icon-arrow-right"></button>',
    });

    $('.carousel').slick({
        slidesToShow: 5,
        arrows: true,
        dots: false,
        prevArrow: '<button type="button" class="slider-nav prev icon-arrow-left"></button>',
        nextArrow: '<button type="button" class="slider-nav next slick-next icon-arrow-right"></button>',
        responsive: [
            {
                breakpoint: 1400,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });
});